import a2s
import json
import requests
import socket
import os
from multiprocessing import Pool, Lock
from pythonping import ping


def init(l):
    global lock
    lock = l

def get_server_list():

    api_url = "{0}servers?limit=1000".format(api_url_base)
    
    response = requests.get(api_url, headers = headers)    
    if response.status_code == 200:
        print('OK response from API')
        return json.loads(response.content.decode('utf-8'))
    else:
        return None

def server_fetchinfo(server):
    server_ip = server['ip'].strip()
    server_port = server['port']            
    
    server_ping = ping(server_ip).rtt_avg_ms
    if server_ping > 2 and server_ping <= 100:                
        server_address = (server_ip,server_port)
        print("Fetching server info for {0}, {1}".format(server_address[0], server_address[1]))
               
        try:
            server_info = a2s.info(server_address)
            if server_info.app_id == 730:
                server_name = server_info.server_name.strip()
                print("{3}, {0}:{1}, {2}".format(server_ip, server_port, server_ping, server_name))
                with open("servers.txt", "a+", encoding='utf-8') as f:
                    # critical section 
                    lock.acquire() 
                    f.write("{3}, {0}:{1}, {2}\n".format(server_ip, server_port, server_ping, server_name))                    
        except socket.timeout as e:                
            pass
        except Exception as e:
            print(e)
            pass
        finally:
            lock.release()


if __name__ == "__main__":
    lock = Lock()
    
    api_url_base = 'http://kztimerglobal.com/api/v1.0/'
    headers = {'Content-Type': 'application/json'}

    server_list = get_server_list()

    if server_list is not None:         
        if os.path.exists("servers.txt"):
            os.remove("servers.txt")           
        p = Pool(initializer=init, initargs=(lock,))
        p.map_async(server_fetchinfo, server_list)
        p.close()
        p.join()            
    else:
        print('[!] Request Failed')

