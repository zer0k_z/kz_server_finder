# README #

A simple multithreaded python script to find global KZ servers around you. 

It simply pull a list of global server IPs from GlobalAPI, check the latency to that address then searches for the server information if the latency is under 100ms. The server information will be saved under *servers.txt*

This is what I used to create my own spreadsheet for my server list [here](https://docs.google.com/spreadsheets/d/1noeRJYNT_JUtWBG0s4UMAw-ASkKjQsfnaw-lmqXINdg/edit?usp=sharing)

### Dependencies ###

* python3 and the default libraries (obviously)
* [python-a2s](https://pypi.org/project/python-a2s/)
* [pythonping](https://pypi.org/project/pythonping/)
* [requests](https://pypi.org/project/requests/)


### Known issues ###
* Some servers are missing: Happens when the actual server IP does not match the IP registered in the API, or the server is down when the script was executed. In rare cases, you have a connection issue to the destination IP.
* Servers are not KZ servers: It might be originally a global KZ server but it was repurposed or the KZ server's IP changed like the previous issue.
* Duplicate entries: Happens sometimes when there are two identical entries in the API. 

### Problems? ###

If you have any question, mention me zer0.k#2613 or go on the [my Discord channel](https://discord.gg/d79CR3M). Or send me a message through Steam, that works too.